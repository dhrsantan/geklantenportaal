{{--<!doctype html>--}}
{{--<html lang="{{ app()->getLocale() }}">--}}
    {{--<head>--}}
        {{--<title>Administratorpaneel</title>--}}
    {{--</head>--}}
    {{--<body>--}}

    @extends('layouts.app')

    @section('content')
        <h1>Alle klanten</h1>
        <p>Hieronder volgt een tabel van alle geregistreerde gebruikers.</p>

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#aaa;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}
.tg .tg-l711{border-color:inherit}
.tg .tg-us36{border-color:inherit;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-l711">Klantnummer</th>
    <th class="tg-l711">Voornaam</th>
    <th class="tg-l711">Tussenvoegsel</th>
    <th class="tg-yw4l">Achternaam</th>
    <th class="tg-yw4l">Telefoonnummer mobiel</th>
    <th class="tg-yw4l">Telefoonnummer vast</th>
    <th class="tg-yw4l">Postcode</th>
    <th class="tg-yw4l">Huisnummer</th>
  </tr>
  <tr>
    <td class="tg-l711">1</td>
    <td class="tg-l711">Piet</td>
    <td class="tg-l711"></td>
    <td class="tg-yw4l">Jan</td>
    <td class="tg-yw4l">+31 6 123 456 78</td>
    <td class="tg-yw4l">+31 475 123 456</td>
    <td class="tg-yw4l">1234AB</td>
    <td class="tg-yw4l">20</td>
  </tr>
  <tr>
    <td class="tg-us36">2</td>
    <td class="tg-us36">Jan</td>
    <td class="tg-us36">van</td>
    <td class="tg-yw4l">Kruchten</td>
    <td class="tg-yw4l">+ 31 6 864 294 10</td>
    <td class="tg-yw4l">+31 44 292 403</td>
    <td class="tg-yw4l">4269GC</td>
    <td class="tg-yw4l">26</td>
  </tr>
</table>

<p></p>

<button>Informatie aanpassen</button>
<button>Klant verwijderen</button>
<button>Facturen inzien</button>

@endsection



        
    {{--</body>--}}
{{--</html>--}}
