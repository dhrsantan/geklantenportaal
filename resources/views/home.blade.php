@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if($type == 'customer')
                    <div class="card-header">Dashboard - Klantenportaal</div>
                @elseif($type == 'colleague')
                    @if($sub == 'administrator')
                        <div class="card-header">Dashboard - Admin portaal</div>
                    @elseif($sub == 'leverancier')
                        <div class="card-header">Dashboard - Leverancier portaal</div>
                    @elseif($sub == 'it afdeling')
                        <div class="card-header">Dashboard - IT portaal</div>
                    @elseif($sub == 'inkoop')
                        <div class="card-header">Dashboard - Inkoop portaal</div>
                    @elseif($sub == 'finances')
                        <div class="card-header">Dashboard - Fynanciën portaal</div>
                    @elseif(!$sub)
                        <div class="card-header">Dashboard - Systeem portaal</div>
                    @endif
                @endif
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if($type == 'customer')
                        <!-- Show Customer information and stuffs -->
                        <div class="col-md-8">
                            {{-- Gegevens inzien, producten inzien (status), looptijden van processen, product wijziging aanvragen, review product,
                             factuur goed/afkeuren, factuur opvragen, advies inzien--}}
                            <a class="btn btn-primary" href="/user" role="button">Gebruikers gegevens</a>
                            <a class="btn btn-primary" href="/vragen" role="button">Klachten en vragen</a>
                            <a class="btn btn-primary" href="/meter" role="button">Meter informatie</a>
                        </div>
                    @elseif($type == 'colleague')
                        <!-- Show Colleague information and stuffs -->
                        @if(!$sub)
                            <div class="col-md-8">
                                <p>
                                    Ga naar een Administrator om het account in een department te laten zetten.
                                </p>
                            </div>
                        @elseif($sub == 'administrator')
                            <!-- Show all admin stuffs -->
                            <div class="col-md-8">
                                <a class="btn btn-primary" href="/admin/viewcolleagues" role="button">Medewerker gegevens</a>
                                <a class="btn btn-primary" href="/admin/viewusers" role="button">Klanten gegevens</a>
                            </div>
                        @elseif($sub == 'leverancier')
                                <div class="col-md-8">
                                    <p>
                                        Dit is het Leverancier portaal...
                                    </p>
                                    <a class="btn btn-primary" href="#" role="button"></a>
                                    <a class="btn btn-primary" href="#" role="button"></a>
                                    <a class="btn btn-primary" href="#" role="button"></a>
                                    <a class="btn btn-primary" href="#" role="button"></a>
                                </div>
                        @elseif($sub == 'it afdeling')
                                <div class="col-md-8">
                                    <p>
                                        Dit is het IT portaal...
                                    </p>
                                </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
