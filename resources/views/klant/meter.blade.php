@extends('layouts.app')

@section('content')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        google.charts.setOnLoadCallback(drawChart2);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['maand', 'verbruik',],
                ['januari',  37329],
                ['februari',  37839],
                ['maart',  38587],
                ['april',  38932],
                ['mei',  40600],
                ['juni',  40893],
                ['juli',  41232],
                ['augustus',  41712],
            ]);

            var options = {
                title: 'meterstanden per maand',
                curveType: 'function',
                legend: { position: 'bottom' },
                height: 500,
            };

            var chart = new google.visualization.LineChart(document.getElementById('meterChart'));

            chart.draw(data, options);
        }

        function drawChart2() {
            var data = google.visualization.arrayToDataTable([
                ['maand', 'verbruik',{ role: "style" }],
                ['januari',  547, 'color: #76A7FA'],
                ['februari',  510, 'color: #76A7FA'],
                ['maart',  748, 'color: #76A7FA'],
                ['april',  1668, 'color: #76A7FA'],
                ['mei',  293, 'color: #76A7FA'],
                ['juni',  339, 'color: #76A7FA'],
                ['juli',  480, 'color: #76A7FA'],
            ]);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                { calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation" },
                2]);

            var options = {
                title: "Density of Precious Metals, in g/cm^3",
                height: 400,
                bar: {groupWidth: "95%"},
                legend: { position: "none" },
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("verbruikChart"));
            chart.draw(view, options);
        }
    </script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">

                    <div class="card-header">Dashboard - Klantenportaal</div>

                <div class="card-body">
                               <div id="verbruikChart"></div>
                            <br/>
                                <ul>
                                    <li>Gem Verbruik: 547.875</li>
                                </ul>
                </div>
            </div>
        </div>
       <div class="col-md-2">
           <div class="card">

               <div class="card-header">verbruik top 5</div>

               <div class="card-body">
                       <ul>
                           <li>1 - 423.239</li>
                           <li>2 - 450.304</li>
                           <li>3 - 503.271</li>
                           <li class="bg-success">4 - 547.875</li>
                           <li>5 - 632.392</li>
                       </ul>

               </div>
           </div>
       </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Charts</b></div>
                <div class="panel-body">
                    <div id="meterChart" ></div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

