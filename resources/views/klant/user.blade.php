{{--<!doctype html>--}}
{{--<html lang="{{ app()->getLocale() }}">--}}
{{--<head>--}}
{{--<title>Administratorpaneel</title>--}}
{{--</head>--}}
{{--<body>--}}

@extends('layouts.app')

@section('content')

    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#aaa;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}
        .tg .tg-l711{border-color:inherit}
        .tg .tg-us36{border-color:inherit;vertical-align:top}
        .tg .tg-yw4l{vertical-align:top}
    </style>
  <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">

                    <div class="card-header">Gebruikersinformatie</div>

                    <div class="card-body">
                        <div class="row">

                                <ul >
                                    <li>Klantnummer</li>
                                    <li>Voornaam</li>
                                    <li>Achternaam</li>
                                    <li>Telefoonnummer mobiel</li>
                                    <li>Telefoonnummer vast</li>
                                    <li>Postcode</li>
                                    <li>Huisnummer</li>
                                </ul>
                                <ul class="list-unstyled">
                                    <li>1</li>
                                    <li>Piet</li>
                                    <li>Jan</li>
                                    <li>+31 6 123 456 78</li>
                                    <li>+31 475 123 456</li>
                                    <li>1234AB</li>
                                    <li>20</li>
                                </ul>
                            <br/>

                        </div>
                        <button>Informatie aanpassen</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




{{--</body>--}}
{{--</html>--}}
