<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $customer = DB::table('customers')->where('user_id', $user['id'])->count();
        $colleague = DB::table('colleagues')->where('user_id', $user['id'])->count();

        if($customer > 0) {
            $type = 'customer';
            $sub = 'customer';
        }
        elseif($colleague > 0){
            $dep = DB::table('colleagues')->where('user_id', $user['id'])->value('department');

            $type = 'colleague';
            $sub = strtolower($dep);
        }

        return view('home', compact('type', 'sub'));
    }
}
