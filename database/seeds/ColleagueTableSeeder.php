<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ColleagueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colleagues')->insert([
            'user_id' => 1,
            'department' => 'Administrator'
        ]);
        
        DB::table('colleagues')->insert([
            'user_id' => 2,
            'department' => 'Administrator'
        ]);

        DB::table('colleagues')->insert([
            'user_id' => 3,
            'department' => 'Administrator'
        ]);
    }
}
