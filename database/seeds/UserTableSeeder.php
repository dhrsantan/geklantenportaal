<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'daan',
            'email' => 'mascha_ba@live.nl',
            'password' => bcrypt('administrator')
        ]);
        
        DB::table('users')->insert([
            'username' => 'dominique',
            'email' => 'dominiquedewaardt@gmail.com',
            'password' => bcrypt('moderator')
        ]);

        DB::table('users')->insert([
            'username' => 'aborn',
            'email' => 'dhrsantan@gmail.nl',
            'password' => bcrypt('admin')
        ]);

        DB::table('users')->insert([
            'username' => 'klant1',
            'email' => 'test@test.test',
            'password' => bcrypt('klant1')
        ]);
    }
}
