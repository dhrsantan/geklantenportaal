<?php

use Illuminate\Database\Seeder;

class AddCustomerDetails extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'user_id' => 4,
            'first_name' => 'First Name',
            'middle_name' => null,
            'last_name' => 'Last Name',
            'country' => 'A Country',
            'city' => 'A City',
            'postal_code' => 'A Postal Code',
            'house_number' => 'A House Number',
            'telephone_number' => 'A Telephone Number'
        ]);
    }
}
