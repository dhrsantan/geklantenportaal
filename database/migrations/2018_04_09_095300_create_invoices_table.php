<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->dateTime('payed_at')->nullable();
            $table->string('version');
            $table->integer('edited_by')->nullable()->unsigned(); // Foreign key Colleague ID
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('edited_by')->references('id')->on('colleagues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
