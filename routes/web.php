<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/meter', function () {
    return view('klant/meter');
});

Route::get('/user', function () {
    return view('klant/user');
});

Route::get('/vragen', function () {
    return view('procespage/aanvragen');
});
Route::get('/admin/panel', function () {
    return view('adminpanel/adminpanel');
});

Route::get('/admin/viewusers', function () {
    return view('adminpanel/viewusers');
});

Route::get('/admin/viewcolleagues', function () {
    return view('adminpanel/viewcolleagues');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
